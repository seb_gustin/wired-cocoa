//
//  UPreferences.h
//  DicomX
//
//  Created by nark on 20/03/11.
//  Copyright 2011 Read-Write. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WPPortChecker.h"


@class WPAccountManager, WPConfigManager, WPExportManager;
@class WPWiredManager, WPPortChecker, WPLogManager;


@interface WSSettingsController : NSWindowController <NSToolbarDelegate>

/** UI Outlets */
@property (assign) IBOutlet NSToolbar                   *toolbar;

@property (assign) IBOutlet NSView                      *generalPreferenceView;
@property (assign) IBOutlet NSView                      *networkPreferenceView;
@property (assign) IBOutlet NSView                      *advancedPreferenceView;
@property (assign) IBOutlet NSView                      *logsPreferenceView;
@property (assign) IBOutlet NSView                      *updatePreferenceView;

@property (assign) IBOutlet NSTextField                 *versionTextField;
@property (assign) IBOutlet NSButton					*installButton;
@property (assign) IBOutlet NSProgressIndicator         *installProgressIndicator;
@property (assign) IBOutlet NSButton					*revealButton;

@property (assign) IBOutlet NSImageView                 *statusImageView;
@property (assign) IBOutlet NSTextField                 *statusTextField;
@property (assign) IBOutlet NSButton					*startButton;
@property (assign) IBOutlet NSProgressIndicator         *startProgressIndicator;

@property (assign) IBOutlet NSButton					*launchAutomaticallyButton;
@property (assign) IBOutlet NSButton					*enableStatusMenuyButton;

@property (assign) IBOutlet NSTableView                 *logTableView;
@property (assign) IBOutlet NSTableColumn				*logTableColumn;
@property (assign) IBOutlet NSButton					*openLogButton;

@property (assign) IBOutlet NSPopUpButton				*filesPopUpButton;
@property (assign) IBOutlet NSMenuItem					*filesMenuItem;

@property (assign) IBOutlet NSTextField                 *portTextField;
@property (assign) IBOutlet NSImageView                 *portStatusImageView;
@property (assign) IBOutlet NSTextField                 *portStatusTextField;
@property (assign) IBOutlet NSButton                    *mapPortAutomaticallyButton;
@property (assign) IBOutlet NSButton                    *checkPortAgainButton;

@property (assign) IBOutlet NSTextField                 *accountStatusTextField;
@property (assign) IBOutlet NSImageView                 *accountStatusImageView;
@property (assign) IBOutlet NSButton					*setPasswordButton;
@property (assign) IBOutlet NSButton					*createAdminButton;
@property (assign) IBOutlet NSButton					*setPasswordForAdminButton;
@property (assign) IBOutlet NSButton					*createNewAdminUserButton;
@property (assign) IBOutlet NSTextField					*snapshotTextField;

@property (assign) IBOutlet NSButton					*exportSettingsButton;
@property (assign) IBOutlet NSButton					*importSettingsButton;

@property (assign) IBOutlet NSButton					*automaticallyCheckForUpdate;

@property (assign) IBOutlet NSPanel                     *passwordPanel;
@property (assign) IBOutlet NSSecureTextField			*newPasswordTextField;
@property (assign) IBOutlet NSSecureTextField			*verifyPasswordTextField;
@property (assign) IBOutlet NSTextField                 *passwordMismatchTextField;

/** Attributes */
@property (readwrite, retain) SUUpdater					*updater;

@property (readwrite, retain) WPAccountManager			*accountManager;
@property (readwrite, retain) WPConfigManager			*configManager;
@property (readwrite, retain) WPExportManager			*exportManager;
@property (readwrite, retain) WPLogManager				*logManager;
@property (readwrite, retain) WPWiredManager			*wiredManager;

@property (readwrite, retain) WPPortChecker				*portChecker;
@property (readwrite)         WPPortCheckerStatus       portCheckerStatus;
@property (readwrite)         NSUInteger				portCheckerPort;

@property (readwrite)         NSInteger                 currentViewTag;

@property (readwrite, retain) NSImage					*greenDropImage;
@property (readwrite, retain) NSImage					*redDropImage;
@property (readwrite, retain) NSImage					*grayDropImage;

@property (readwrite, retain) WIDateFormatter           *dateFormatter;
@property (readwrite, retain) NSMutableArray            *logLines;
@property (readwrite, retain) NSMutableArray            *logRows;
@property (readwrite, retain) NSDictionary              *logAttributes;


- (IBAction)switchView:(id)sender;

- (IBAction)install:(id)sender;
- (IBAction)uninstall:(id)sender;
- (IBAction)releaseNotes:(id)sender;

- (IBAction)checkForUpdate:(id)sender;
- (IBAction)automaticallyCheckForUpdate:(id)sender;

- (IBAction)start:(id)sender;
- (IBAction)stop:(id)sender;

- (IBAction)launchAutomatically:(id)sender;
- (IBAction)enableStatusMenuItem:(id)sender;

- (IBAction)openLog:(id)sender;
- (IBAction)crashReports:(id)sender;

- (IBAction)other:(id)sender;
- (IBAction)reveal:(id)sender;

- (IBAction)port:(id)sender;
- (IBAction)mapPortAutomatically:(id)sender;
- (IBAction)checkPortAgain:(id)sender;

- (IBAction)setPasswordForAdmin:(id)sender;
- (IBAction)createNewAdminUser:(id)sender;
- (IBAction)submitPasswordSheet:(id)sender;

- (IBAction)exportSettings:(id)sender;
- (IBAction)importSettings:(id)sender;

@end
