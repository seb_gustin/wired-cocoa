//
//  WBBotManager.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 05/04/12.
//  Copyright (c) 2012 OPALE. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WBError;

@interface WBBotManager : NSObject

@property (readwrite, getter = isRunning)       BOOL        running;
@property (readwrite, getter = isRunning)       NSInteger   pid;

- (BOOL)startWithError:(WBError **)error;
- (BOOL)stopWithError:(WBError **)error;

@end
