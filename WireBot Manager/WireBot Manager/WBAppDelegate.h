//
//  WBAppDelegate.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 05/04/12.
//  Copyright (c) 2012 OPALE. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WBBotManager;

@interface WBAppDelegate : NSObject <NSApplicationDelegate, NSMenuDelegate>

@property (assign)              IBOutlet NSMenu                 *statusMenu;
@property (readwrite, retain)   NSStatusItem                    *statusItem;
@property (readwrite, retain)   WBBotManager                    *botManager;

- (IBAction)start:(id)sender;
- (IBAction)stop:(id)sender;
- (IBAction)editConfig:(id)sender;

@end
