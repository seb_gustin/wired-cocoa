//
//  WBAppDelegate.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 05/04/12.
//  Copyright (c) 2012 OPALE. All rights reserved.
//

#import "WBAppDelegate.h"
#import "WBBotManager.h"
#import "WBError.h"


@interface WBAppDelegate (Private)
- (void)_updateStatusMenu;
@end



@implementation WBAppDelegate


#pragma mark -

@synthesize statusItem          = _statusItem;
@synthesize statusMenu          = _statusMenu;
@synthesize botManager          = _botManager;


#pragma mark -

- (id)init {
    self = [super init];
    if (self) {
        _statusItem = [[[NSStatusBar systemStatusBar] statusItemWithLength:30.0] retain];
        _botManager = [[WBBotManager alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_statusItem release];
    [_statusMenu release];
    [_botManager release];
    [super dealloc];
}


#pragma mark -

- (void)applicationDidFinishLaunching:(NSNotification *)notification {
    
    [self.statusItem setHighlightMode:YES];
    [self.statusItem setMenu:self.statusMenu];
    
    [self _updateStatusMenu];
}

- (BOOL)applicationShouldOpenUntitledFile:(NSApplication *)sender {
    return NO;
}



#pragma mark -

- (IBAction)start:(id)sender {
    WBError		*error;
    [_botManager startWithError:&error];
    
    [self _updateStatusMenu];
}

- (IBAction)stop:(id)sender {
    WBError		*error;
    [_botManager stopWithError:&error];
    
    [self _updateStatusMenu];
}

- (IBAction)editConfig:(id)sender {
    [[NSWorkspace sharedWorkspace] openFile:[WB_USER_PATH stringByAppendingPathComponent:@"wirebot.conf"]
                            withApplication:@"TextEdit"];
}



#pragma mark -

- (void)menuWillOpen:(NSMenu *)menu {
    
    NSMenuItem      *item;
    
    [menu removeAllItems];
    
    if(![_botManager isRunning]) {
        item = [menu addItemWithTitle:@"Start Wirebot…" action:@selector(start:) keyEquivalent:@""];
        [item setTarget:self];
    } else {
        item = [menu addItemWithTitle:@"Stop Wirebot…" action:@selector(stop:) keyEquivalent:@""];
        [item setTarget:self];
    }
    
    [menu addItem:[NSMenuItem separatorItem]];
    item = [menu addItemWithTitle:@"Edit Wirebot Config…" action:@selector(editConfig:) keyEquivalent:@""];
    [item setTarget:self];
    
    [menu addItem:[NSMenuItem separatorItem]];
    item = [menu addItemWithTitle:@"Edit a Wirebot Dictionary…" action:nil keyEquivalent:@""];
    [item setTarget:self];
}



#pragma mark -

- (void)_updateStatusMenu {
    if([_botManager isRunning]) {
        [self.statusItem setImage:[NSImage imageNamed:@"WiredServerMenu"]];
	} else {
        [self.statusItem setImage:[NSImage imageNamed:@"WiredServerMenuOff"]];
    }
}


@end
