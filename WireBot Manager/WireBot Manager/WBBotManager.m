//
//  WBBotManager.m
//  WireBot Manager
//
//  Created by Rafaël Warnault on 05/04/12.
//  Copyright (c) 2012 OPALE. All rights reserved.
//

#import "WBBotManager.h"
#import "WBError.h"

#import <signal.h>

@interface WBBotManager (Private)
- (BOOL)_reloadPidFile;
@end



@implementation WBBotManager


@synthesize running = _running;
@synthesize pid     = _pid;

- (id)init {
    self = [super init];
    if (self) {
        [self _reloadPidFile];
    }
    return self;
}


- (BOOL)startWithError:(WBError **)error {
    NSTask			*task;
	
	task = [[[NSTask alloc] init] autorelease];
	[task setLaunchPath:WB_BINARY_PATH];
	[task setStandardOutput:[NSPipe pipe]];
	[task setStandardError:[task standardOutput]];
	[task launch];
	
    self.running = YES;
    
	return YES;
}

- (BOOL)stopWithError:(WBError **)error {
    
    [self _reloadPidFile];
    
    kill((int)_pid, SIGQUIT);

    
    self.running = NO;
    return YES;
}



- (BOOL)_reloadPidFile {
	NSString		*string, *command;
	BOOL			running = NO;
	
	string = [NSString stringWithContentsOfFile:[WB_USER_PATH stringByAppendingPathComponent:@"wirebot.pid"]
									   encoding:NSUTF8StringEncoding
										  error:NULL];
	
	if(string) {
		command = [[NSWorkspace sharedWorkspace] commandForProcessIdentifier:[string unsignedIntValue]];
		
		if([command isEqualToString:@"wirebot"]) {
			_pid = [string unsignedIntegerValue];
			
			running = YES;
		} else {
            [[NSFileManager defaultManager] removeItemAtPath:[WB_USER_PATH stringByAppendingPathComponent:@"wirebot.pid"]
                                                       error:nil];
		}
	}
    
    NSLog(@"pid: %ld", _pid);
	
	if(running != _running) {
		_running = running;
		
		return YES;
	}
	
	return NO;
}



@end
