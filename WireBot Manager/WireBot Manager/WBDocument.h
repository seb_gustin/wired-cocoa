//
//  WBDocument.h
//  WireBot Manager
//
//  Created by Rafaël Warnault on 05/04/12.
//  Copyright (c) 2012 OPALE. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface WBDocument : NSDocument

@end
