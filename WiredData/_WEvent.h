// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to WEvent.h instead.

#import <CoreData/CoreData.h>


@class WServer;






@interface WEventID : NSManagedObjectID {}
@end

@interface _WEvent : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (WEventID*)objectID;




@property (nonatomic, retain) NSDate *eventDate;


//- (BOOL)validateEventDate:(id*)value_ error:(NSError**)error_;




@property (nonatomic, retain) NSString *eventDescription;


//- (BOOL)validateEventDescription:(id*)value_ error:(NSError**)error_;




@property (nonatomic, retain) NSString *eventTitle;


//- (BOOL)validateEventTitle:(id*)value_ error:(NSError**)error_;




@property (nonatomic, retain) NSNumber *eventType;


@property int eventTypeValue;
- (int)eventTypeValue;
- (void)setEventTypeValue:(int)value_;

//- (BOOL)validateEventType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, retain) WServer* server;

//- (BOOL)validateServer:(id*)value_ error:(NSError**)error_;




@end

@interface _WEvent (CoreDataGeneratedAccessors)

@end

@interface _WEvent (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveEventDate;
- (void)setPrimitiveEventDate:(NSDate*)value;




- (NSString*)primitiveEventDescription;
- (void)setPrimitiveEventDescription:(NSString*)value;




- (NSString*)primitiveEventTitle;
- (void)setPrimitiveEventTitle:(NSString*)value;




- (NSNumber*)primitiveEventType;
- (void)setPrimitiveEventType:(NSNumber*)value;

- (int)primitiveEventTypeValue;
- (void)setPrimitiveEventTypeValue:(int)value_;





- (WServer*)primitiveServer;
- (void)setPrimitiveServer:(WServer*)value;


@end
