// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to WServer.m instead.

#import "_WServer.h"

@implementation WServerID
@end

@implementation _WServer

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Server" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Server";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Server" inManagedObjectContext:moc_];
}

- (WServerID*)objectID {
	return (WServerID*)[super objectID];
}

+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"downloadSpeedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"downloadSpeed"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}
	if ([key isEqualToString:@"downloadsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"downloads"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}
	if ([key isEqualToString:@"numberOfFilesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"numberOfFiles"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}
	if ([key isEqualToString:@"sizeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"size"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}
	if ([key isEqualToString:@"supportRsrcValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"supportRsrc"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}
	if ([key isEqualToString:@"uploadSpeedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"uploadSpeed"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}
	if ([key isEqualToString:@"uploadsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"uploads"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}

	return keyPaths;
}




@dynamic address;






@dynamic banner;






@dynamic downloadSpeed;



- (int)downloadSpeedValue {
	NSNumber *result = [self downloadSpeed];
	return [result intValue];
}

- (void)setDownloadSpeedValue:(int)value_ {
	[self setDownloadSpeed:[NSNumber numberWithInt:value_]];
}

- (int)primitiveDownloadSpeedValue {
	NSNumber *result = [self primitiveDownloadSpeed];
	return [result intValue];
}

- (void)setPrimitiveDownloadSpeedValue:(int)value_ {
	[self setPrimitiveDownloadSpeed:[NSNumber numberWithInt:value_]];
}





@dynamic downloads;



- (int)downloadsValue {
	NSNumber *result = [self downloads];
	return [result intValue];
}

- (void)setDownloadsValue:(int)value_ {
	[self setDownloads:[NSNumber numberWithInt:value_]];
}

- (int)primitiveDownloadsValue {
	NSNumber *result = [self primitiveDownloads];
	return [result intValue];
}

- (void)setPrimitiveDownloadsValue:(int)value_ {
	[self setPrimitiveDownloads:[NSNumber numberWithInt:value_]];
}





@dynamic lastConnectDate;






@dynamic login;






@dynamic numberOfFiles;



- (long long)numberOfFilesValue {
	NSNumber *result = [self numberOfFiles];
	return [result longLongValue];
}

- (void)setNumberOfFilesValue:(long long)value_ {
	[self setNumberOfFiles:[NSNumber numberWithLongLong:value_]];
}

- (long long)primitiveNumberOfFilesValue {
	NSNumber *result = [self primitiveNumberOfFiles];
	return [result longLongValue];
}

- (void)setPrimitiveNumberOfFilesValue:(long long)value_ {
	[self setPrimitiveNumberOfFiles:[NSNumber numberWithLongLong:value_]];
}





@dynamic password;






@dynamic preferredNick;






@dynamic preferredStatus;






@dynamic serverDescription;






@dynamic serverName;






@dynamic size;



- (long long)sizeValue {
	NSNumber *result = [self size];
	return [result longLongValue];
}

- (void)setSizeValue:(long long)value_ {
	[self setSize:[NSNumber numberWithLongLong:value_]];
}

- (long long)primitiveSizeValue {
	NSNumber *result = [self primitiveSize];
	return [result longLongValue];
}

- (void)setPrimitiveSizeValue:(long long)value_ {
	[self setPrimitiveSize:[NSNumber numberWithLongLong:value_]];
}





@dynamic startTime;






@dynamic supportRsrc;



- (BOOL)supportRsrcValue {
	NSNumber *result = [self supportRsrc];
	return [result boolValue];
}

- (void)setSupportRsrcValue:(BOOL)value_ {
	[self setSupportRsrc:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveSupportRsrcValue {
	NSNumber *result = [self primitiveSupportRsrc];
	return [result boolValue];
}

- (void)setPrimitiveSupportRsrcValue:(BOOL)value_ {
	[self setPrimitiveSupportRsrc:[NSNumber numberWithBool:value_]];
}





@dynamic uploadSpeed;



- (int)uploadSpeedValue {
	NSNumber *result = [self uploadSpeed];
	return [result intValue];
}

- (void)setUploadSpeedValue:(int)value_ {
	[self setUploadSpeed:[NSNumber numberWithInt:value_]];
}

- (int)primitiveUploadSpeedValue {
	NSNumber *result = [self primitiveUploadSpeed];
	return [result intValue];
}

- (void)setPrimitiveUploadSpeedValue:(int)value_ {
	[self setPrimitiveUploadSpeed:[NSNumber numberWithInt:value_]];
}





@dynamic uploads;



- (int)uploadsValue {
	NSNumber *result = [self uploads];
	return [result intValue];
}

- (void)setUploadsValue:(int)value_ {
	[self setUploads:[NSNumber numberWithInt:value_]];
}

- (int)primitiveUploadsValue {
	NSNumber *result = [self primitiveUploads];
	return [result intValue];
}

- (void)setPrimitiveUploadsValue:(int)value_ {
	[self setPrimitiveUploads:[NSNumber numberWithInt:value_]];
}





@dynamic events;

	
- (NSMutableSet*)eventsSet {
	[self willAccessValueForKey:@"events"];
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"events"];
	[self didAccessValueForKey:@"events"];
	return result;
}
	

@dynamic privateMessages;

	
- (NSMutableSet*)privateMessagesSet {
	[self willAccessValueForKey:@"privateMessages"];
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"privateMessages"];
	[self didAccessValueForKey:@"privateMessages"];
	return result;
}
	

@dynamic publicChat;

	





@end
