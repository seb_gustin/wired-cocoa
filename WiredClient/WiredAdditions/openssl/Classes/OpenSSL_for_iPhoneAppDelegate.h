//
//  OpenSSL_for_iPhoneAppDelegate.h
//  OpenSSL-for-iPhone
//
//  Created by Felix Schulze on 01.02.2010.
//  Copyright Felix Schulze 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenSSL_for_iPhoneAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@end

