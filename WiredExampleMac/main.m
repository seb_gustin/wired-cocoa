//
//  main.m
//  WiredExampleMac
//
//  Created by Rafaël Warnault on 16/12/11.
//  Copyright (c) 2011 Read-Write. All rights reserved.
//


#define SERVER_URL      @"my.server.org"
#define USER_LOGIN      @"guest"
#define USER_PWD        @"password"
#define USER_NICK       @"WiredExampleMac"
#define USER_STATUS     @"Xcode Resident"


NSManagedObjectModel *managedObjectModel(void);
NSManagedObjectContext *managedObjectContext(void);

int main (int argc, const char * argv[])
{

        @autoreleasepool {
            
            // *** Core Data is not used here
            // Create the managed object context
            // NSManagedObjectContext *context = managedObjectContext();
            
            WIError *error = nil;
            
            // load the Wired specification
            NSString *execPath = [[[NSProcessInfo processInfo] arguments] objectAtIndex:0];
            NSString *specPath = [[execPath stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"wired.xml"];
            WCP7Spec = [[WIP7Spec alloc] initWithPath:specPath 
                                           originator:WIP7Client 
                                                error:&error];
            if(error) {
                NSLog(@"ERROR : %@ (%@)", [error localizedDescription], [error localizedFailureReason]);
                return 0;
            }
            // create a new wired connection
            __block WConnection *connection = [[WConnection alloc] initWithURL:SERVER_URL 
                                                                 login:USER_LOGIN 
                                                              password:USER_PWD];
            // add user setup (optional)
            [connection setNick:USER_NICK];
            [connection setStatus:USER_STATUS];
            
            // try to connect
            [connection connectWithBlock:^(id object) {
                
                // connection failed
                if([object isKindOfClass:[NSError class]]) {
                    NSLog(@"ERROR : %@ (%@)", [(WIError *)object localizedDescription], [(WIError *)object localizedFailureReason]);
                    
                // connection succeeded
                } else if([object isKindOfClass:[WLink class]]) {
                    NSLog(@"INFO : Connected to %@", SERVER_URL);
                    
                    // try to login
                    [connection loginWithBlock:^(WIP7Message *response, NSError *error) {
                        if(error) {
                            NSLog(@"ERROR : %@ (%@)", [error localizedDescription], [error localizedFailureReason]);
                        }
                        
                        if(response) {
                            NSLog(@"INFO : Logged as %@", USER_LOGIN);
                            NSLog(@"INFO : %@", [response description]);
                            
                            // send a chat message
                            [connection sendChatSayMessage:@"Hello, world !" 
                                                withChatID:1 /* (1 == public_chat) */
                                                 withBlock:^(WIP7Message *response, NSError *error) {
                                                     if(response) {
                                                         NSLog(@"INFO : Message sent to public chat");
                                                         NSLog(@"INFO : %@", [response description]);
                                                     }
                                                 }];
                        }
                    }];
                }
            }];

            // Start a runloop because WiredKit needs one
            // NOTE : Not needed for NSApplication or UIApplication instances
            [[NSRunLoop currentRunLoop] run];

            // *** Here we don't want to save, it's an example
            // Custom code here...
            // Save the managed object context
//            NSError *error = nil;    
//            if (![context save:&error]) {
//                NSLog(@"Error while saving %@", ([error localizedDescription] != nil) ? [error localizedDescription] : @"Unknown Error");
//                exit(1);
//            }
        }
    return 0;
}



NSManagedObjectModel *managedObjectModel() {
    
    static NSManagedObjectModel *model = nil;
    
    if (model != nil) {
        return model;
    }
    
    // *** Here we use WiredData framework to load the mbbed Core Data model (not used, just for example)
    model = [[WiredData managedObjectModel] retain];
    return model;
}

NSManagedObjectContext *managedObjectContext() {

    static NSManagedObjectContext *context = nil;
    if (context != nil) {
        return context;
    }

    @autoreleasepool {
        context = [[NSManagedObjectContext alloc] init];
        
        NSPersistentStoreCoordinator *coordinator = [[[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:managedObjectModel()] autorelease];
        [context setPersistentStoreCoordinator:coordinator];
        
        NSString *STORE_TYPE = NSSQLiteStoreType;
        
        NSString *path = [[[NSProcessInfo processInfo] arguments] objectAtIndex:0];
        path = [path stringByDeletingPathExtension];
        NSURL *url = [NSURL fileURLWithPath:[path stringByAppendingPathExtension:@"sqlite"]];
        
        NSError *error;
        NSPersistentStore *newStore = [coordinator addPersistentStoreWithType:STORE_TYPE configuration:nil URL:url options:nil error:&error];
        
        if (newStore == nil) {
            NSLog(@"Store Configuration Failure %@", ([error localizedDescription] != nil) ? [error localizedDescription] : @"Unknown Error");
        }
    }
    return context;
}

